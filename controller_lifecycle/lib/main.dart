import 'package:flutter/material.dart';
import 'package:controller_lifecycle/my_controller.dart';
import 'package:get/get.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyController myController = Get.put(MyController());

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'GetX controller Life Cycle',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            "GetX controller Lifecycle",
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GetBuilder<MyController>(
                builder: (controller) {
                  // initState:
                  // (data) => MyController().increment();
                  // dispose:
                  // (_) => MyController().cleanUpTask();
                  return Text(
                    "The value is ${controller.count}",
                    style: TextStyle(
                      fontSize: 26,
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
