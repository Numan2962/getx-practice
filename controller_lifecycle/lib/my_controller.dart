import 'package:get/get.dart';

class MyController extends GetxController {
  var count = 2;

  void increment() async {
    var duration = Duration(seconds: 2);
    await Future<int>.delayed(duration);
    this.count++;
    update();
  }

  void cleanUpTask() {
    print("Clean up task");
  }

  @override
  void onInit() {
    print("Init Called");
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
