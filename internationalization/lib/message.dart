import 'package:get/get.dart';

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          'msg': 'How are you?',
        },
        'bn_BN': {
          'msg': 'আপনি কেমন আছেন?',
        },
        'it_IT': {
          'msg': 'Come stai?',
        },
      };
}
