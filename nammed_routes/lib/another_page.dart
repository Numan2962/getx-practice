import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AnotherPage extends StatelessWidget {
  const AnotherPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Another Page"),
      ),
      body: Center(
        child: Text(
          "This is another screen",
          style: TextStyle(color: Colors.red, fontSize: 26),
        ),

        // child: Text(
        //   "${Get.parameters['someValue']}",
        //   style: TextStyle(
        //     color: Colors.red,
        //     fontSize: 26,
        //   ),
        // ),
      ),
    );
  }
}
