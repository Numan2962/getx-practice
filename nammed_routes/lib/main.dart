import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nammed_routes/home.dart';
import 'package:nammed_routes/unknown_route.dart';
import 'another_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Navigation',
      initialRoute: '/',
      defaultTransition: Transition.zoom,
      getPages: [
        GetPage(name: '/', page: () => MyApp()),
        GetPage(name: '/home', page: () => HomePage()),
        GetPage(
          name: '/another',
          page: () => AnotherPage(),
          transition: Transition.leftToRight,
        ),
        // GetPage(
        //   name: '/another/:someValue',
        //   page: () => AnotherPage(),
        //   transition: Transition.leftToRight,
        // ),
      ],
      unknownRoute: GetPage(name: '/notfound', page: () => UnknownRoute()),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Nammed routes"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RaisedButton(
                child: Text("Go to Home"),
                onPressed: () {
                  Get.toNamed(
                    "/home",
                  );

                  // Get.toNamed(
                  //   "/home?channel=Ripples Code&content=Flutter Getx",
                  // );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
