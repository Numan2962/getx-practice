import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  var count = 0.obs;

  void increment() {
    count++;
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Reactive State Management',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Reactive State Management"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Obx(() => Text(
                    "Count value is $count",
                    style: TextStyle(
                      fontSize: 26,
                    ),
                  )),
              SizedBox(
                height: 16,
              ),
              RaisedButton(
                  child: Text(
                    'Increment',
                  ),
                  onPressed: () {
                    increment();
                  })
            ],
          ),
        ),
      ),
    );
  }
}
