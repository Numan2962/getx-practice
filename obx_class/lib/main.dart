import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:obx_class/my_controller.dart';
import 'package:obx_class/student.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  //var student = Student();

  //final student = Student(name: "Numan", age: 25).obs;

  // Create the instance of Controller
  MyController myController = Get.put(MyController());

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'State Management',
      home: Scaffold(
        appBar: AppBar(
          title: Text("State Management"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Obx(() => Text(
              //       "Name is ${myController.student.value.name}",
              //       style: TextStyle(
              //         fontSize: 26,
              //       ),
              //     )),
              GetX<MyController>(
                //init: MyController(),
                builder: (controller) {
                  return Text(
                    "The value is ${myController.count}",
                    style: TextStyle(
                      fontSize: 26,
                    ),
                  );
                },
              ),
              SizedBox(
                height: 16,
              ),
              RaisedButton(
                child: Text("Upper"),
                onPressed: () {
                  //student.name.value = student.name.value.toUpperCase();

                  // student.update((student) {
                  //   student!.name = student.name.toString().toUpperCase();
                  // });

                  //myController.convertToUpperCase();

                  myController.increment();

                  // If instance of controller is not created at top
                  //Get.find<MyController>().increment();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
