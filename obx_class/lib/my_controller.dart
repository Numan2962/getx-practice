import 'package:obx_class/student.dart';
import 'package:get/get.dart';

class MyController extends GetxController {
  //var student = Student();
  // void convertToUpperCase() {
  //   student.name.value = student.name.value.toUpperCase();
  // }
  var count = 0.obs;
  void increment() {
    count++;
  }

  var student = Student(name: "Numan", age: 25).obs;
  void convertToUpperCase() {
    student.update((student) {
      student!.name = student.name.toString().toUpperCase();
    });
  }
}
