import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Dialogue',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Dialogue"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RaisedButton(
                child: Text("Show Dialogue"),
                onPressed: () {
                  Get.defaultDialog(
                    title: "Dialog Title",
                    titleStyle: TextStyle(fontSize: 25),
                    middleText: "This is middle text",
                    middleTextStyle: TextStyle(fontSize: 20),
                    backgroundColor: Colors.brown,
                    radius: 85,

                    // Customize the middle text
                    content: Row(
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          width: 12,
                        ),
                        Expanded(
                          child: Text("Data Loading"),
                        ),
                      ],
                    ),
                    textCancel: "Cancel",
                    cancelTextColor: Colors.white,
                    textConfirm: "Confirm",
                    confirmTextColor: Colors.white,
                    onCancel: () {},
                    onConfirm: () {},
                    buttonColor: Colors.green,

                    // cancel: Text(
                    //   "Cancels",
                    //   style: TextStyle(
                    //     color: Colors.white,
                    //   ),
                    // ),
                    // confirm: Text(
                    //   "Confirms",
                    //   style: TextStyle(color: Colors.white),
                    // ),

                    actions: [
                      RaisedButton(
                        child: Text("Action-1"),
                        onPressed: () {
                          Get.back();
                        },
                      ),
                    ],
                    barrierDismissible: false,
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
