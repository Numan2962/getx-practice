import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Snackbar',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Snackbar"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RaisedButton(
                  child: Text("Show Snackbar"),
                  onPressed: () {
                    Get.snackbar(
                      "Snackbar Title", "This is snackbar message",
                      snackPosition: SnackPosition.BOTTOM,
                      // titleText: Text("Another Title"),
                      // messageText: Text(
                      //   "Another Message",
                      //   style: TextStyle(color: Colors.amber),
                      // ),
                      colorText: Colors.white,
                      backgroundColor: Colors.brown,
                      borderRadius: 35,
                      margin: EdgeInsets.all(15),
                      animationDuration: Duration(seconds: 3),
                      backgroundGradient: LinearGradient(
                        colors: [Colors.blue, Colors.red],
                      ),
                      borderColor: Colors.black,
                      borderWidth: 4,
                      boxShadows: [
                        BoxShadow(
                            color: Colors.blue,
                            offset: Offset(5, 15),
                            spreadRadius: 5,
                            blurRadius: 3)
                      ],
                      isDismissible: true,
                      dismissDirection: SnackDismissDirection.HORIZONTAL,
                      forwardAnimationCurve: Curves.bounceInOut,
                      duration: Duration(seconds: 10),
                      icon: Icon(
                        Icons.send,
                        color: Colors.black,
                      ),
                      shouldIconPulse: false,
                      //leftBarIndicatorColor: Colors.white,
                      // onTap: (val) {
                      //   print("Retry Click");
                      // },
                      padding: EdgeInsets.all(50),
                      showProgressIndicator: true,
                      progressIndicatorBackgroundColor: Colors.black,
                      progressIndicatorValueColor:
                          AlwaysStoppedAnimation(Colors.white),
                      reverseAnimationCurve: Curves.bounceInOut,
                      snackbarStatus: (val) {
                        print(val);
                      },
                      userInputForm: Form(
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(),
                            ),
                          ],
                        ),
                      ),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}
