import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unique_id/my_controller.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyController myController = Get.put(MyController());
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Unique Id',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Unique ID"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GetBuilder<MyController>(
                  //id: 'txtcount',
                  builder: (controller) {
                return Text("The value is ${controller.count}");
              }),
              GetBuilder<MyController>(
                  id: 'txtcount',
                  builder: (controller) {
                    return Text("The value is ${controller.count}");
                  }),
              SizedBox(
                height: 8,
              ),
              ElevatedButton(
                child: Text("Increment"),
                onPressed: () => myController.increment(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
