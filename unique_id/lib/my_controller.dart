import 'package:get/get_state_manager/get_state_manager.dart';

import 'package:get/get.dart';

class MyController extends GetxController {
  var count = 0;
  void increment() {
    count++;
    update(['txtcount']);
  }
}
