import 'package:flutter/material.dart';
import 'package:get/get.dart';

class home extends StatelessWidget {
  const home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("This is Home Screen"),
            SizedBox(
              height: 8,
            ),
            RaisedButton(
              child: Text("Next Screen"),
              onPressed: () {},
              color: Colors.lightBlue,
              textColor: Colors.white,
            ),
            SizedBox(
              height: 8,
            ),
            RaisedButton(
              child: Text(
                "Main Screen",
                style: TextStyle(fontSize: 16),
              ),
              color: Colors.lightBlue,
              textColor: Colors.white,
              onPressed: () {
                //Get.back();

                Get.back(result: 'This is from home screen');
              },
            ),
            SizedBox(
              height: 8,
            ),
            // Text(
            //   "${Get.arguments}",
            //   style: TextStyle(color: Colors.green, fontSize: 20),
            // ),
          ],
        ),
      ),
    );
  }
}
