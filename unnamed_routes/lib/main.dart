import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unnamed_routes/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Unnamed Routes',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Navigation"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RaisedButton(
                child: Text("Go to Home"),
                onPressed: () async {
                  // Get.to(
                  //   home(),
                  //   fullscreenDialog: true,
                  //   transition: Transition.zoom,
                  //   duration: Duration(seconds: 2),
                  //   curve: Curves.bounceInOut,
                  // );

                  // no option to return home
                  //Get.off(home());

                  // go to home and cancel all previous screens
                  //Get.offAll(home());

                  //Get.to(home(), arguments: "Data from main");

                  var data = await Get.to(home());
                  print("$data");
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
