import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workers/my_controller.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyController myController = Get.put(MyController());
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Workers',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Workers"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ElevatedButton(
                child: Text("Increment"),
                onPressed: () => myController.increment(),
              ),
              Padding(
                  padding: EdgeInsets.all(16),
                  child: TextField(
                    onChanged: (val) {
                      myController.increment();
                    },
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
