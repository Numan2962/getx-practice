import 'package:get/get.dart';

class MyController extends GetxController {
  var count = 0.obs;

  void increment() {
    count++;
  }

  @override
  void onInit() {
    // called every time when the value of count variable changes
    ever(count, (_) => print(count));

    // called every time when the value of any variable from the list changes
    //everAll([count], (_) => print(count));
    //once(count, (_) => print(count));

    // called every time the use stops typing for 1 second
    // debounce(
    //   count,
    //   (_) => print(count),
    //   time: Duration(seconds: 1),
    // );

    // Ignore all changes within 3 seconds
    // interval(count, (_) => print('Ignore all changes'),
    //     time: Duration(seconds: 3));
    super.onInit();
  }
}
